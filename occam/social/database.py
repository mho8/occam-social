# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import datetime

from occam.databases.manager     import uses, datastore

from occam.objects.database      import ObjectDatabase

@datastore("social")
@uses(ObjectDatabase)
class SocialDatabase:
  """ This manages interactions with the database for the social component.
  """

  def queryComment(self, uuid, key=None):
    """ Forms a query for the given comment identifier.
    """

    comments = sql.Table("comments")

    if key:
      query = comments.select(comments.__getattr__(key))
    else:
      query = comments.select()
    query.where = (comments.uid == uuid)

    return query

  def retrieveComment(self, uuid):
    """ Retrieves the specific comment identified by the given uuid.
    """

    from occam.social.records.comment import CommentRecord

    session = self.database.session()

    query = self.queryComment(uuid)
    self.database.execute(session, query)

    return CommentRecord(self.database.fetch(session))

  def queryComments(self, obj, commentId = None, inReplyTo = None, withReplies=False, fullChain = False, key=None):
    """ Returns the SQL query to retrieve the set of comments for the given object without replies.
    """

    from occam.social.records.comment import CommentRecord

    # This retrieves the id for the requested object
    objectQuery = self.objects.queryObjectRecordFor(obj, key = "id")

    # The WITH query will establish the base comment or comments we are pulling from
    withQuery = sql.With()

    # The base query will pull out either all comments for the object or the requested comment
    baseComment = sql.Table("comments")
    query = baseComment.select(where = (baseComment.internal_object_id.in_(objectQuery)))

    # Pull out just the requested comment
    if commentId:
      query.where = query.where & (baseComment.uid == commentId)

    # Or we pull out only replies to a particular comment
    if inReplyTo:
      query.where = query.where & (baseComment.in_reply_to == inReplyTo)

    if not commentId and not inReplyTo:
      query.where = query.where & (baseComment.in_reply_to == sql.Null)

    if fullChain:
      # When we are looking at the ancestry, the WITH clause will contain
      # the intended comment and the parents of the comment
      ancestorComments = sql.Table("comments")
      join = ancestorComments.select().join(withQuery, "INNER")
      join.condition = (join.left.uid == join.right.in_reply_to)

      joinQuery = join.select(getattr(join.left, '*'))

      query = (query | joinQuery)

    # Otherwise, the WITH will simply hold the requested comment or all comments
    # of the requested object
    withQuery.query = query

    if key:
      mainQuery = withQuery.select(withQuery.__getattr__(key))
    elif withReplies:
      # Adds a column called 'num_replies' that contains the number of replies to each pulled comment
      # And also ensures that the query returns every reply to each of the already queried comments
      repliesComments = sql.Table("comments")
      inReplyComments = sql.Table("comments")
      mainQuery = withQuery.select(getattr(withQuery, '*'), sql.As(repliesComments.select(sql.aggregate.Count(repliesComments.uid), where = (repliesComments.in_reply_to == withQuery.uid) & (repliesComments.internal_object_id.in_(objectQuery))), "num_replies"))
      mainQuery = (mainQuery | inReplyComments.select(getattr(inReplyComments, '*'), sql.As(repliesComments.select(sql.aggregate.Count(repliesComments.uid), where = (repliesComments.in_reply_to == inReplyComments.uid) & (repliesComments.internal_object_id.in_(objectQuery))), "num_replies"),
                                                      where = (inReplyComments.in_reply_to.in_(withQuery.select(withQuery.uid)))))
      # For some reason, we can't have a WITH with a UNION. Pretty silly. Probably a python-sql limitation/bug.
      mainQuery = mainQuery.select()
    else:
      mainQuery = withQuery.select()

    mainQuery.with_ = [withQuery]

    return mainQuery

  def retrieveComments(self, obj, commentId = None, inReplyTo = None, withReplies = False, fullChain = False):
    """ Retrieves the set of comments for the given object without replies.
    """

    from occam.social.records.comment import CommentRecord

    session = self.database.session()

    query = self.queryComments(obj, commentId = commentId, inReplyTo = inReplyTo, withReplies = withReplies, fullChain = fullChain)
    self.database.execute(session, query)

    return [CommentRecord(x) for x in self.database.many(session, size=100)]

  def insertComment(self, obj, person_uuid, person_name, alias, comment, in_reply_to):
    """ Creates a CommentRecord and returns it for the given new comment data.
    """

    from occam.social.records.comment import CommentRecord

    # For the generation of a unique identifier for the comment
    from uuid import uuid4

    session = self.database.session()

    record = CommentRecord()

    comments = sql.Table("comments")

    # Create the query
    objectQuery = self.objects.queryObjectRecordFor(obj, key = "id")
    query = comments.insert()
    query.columns = [comments.internal_object_id,
                     comments.object_revision,
                     comments.uid,
                     comments.commenter_uid,
                     comments.commenter_name,
                     comments.content,
                     comments.anonymous_alias,
                     comments.in_reply_to,
                     comments.create_time]

    # Generate a unique identifier for this comment
    comment_uuid = str(uuid4())

    query.values = sql.Select(columns = [objectQuery,
                                         sql.Literal(obj.revision),
                                         sql.Literal(comment_uuid),
                                         sql.Literal(person_uuid),
                                         sql.Literal(person_name),
                                         sql.Literal(comment),
                                         sql.Literal(alias),
                                         sql.Literal(in_reply_to),
                                         sql.Literal(datetime.datetime.utcnow())])

    # Insert the row
    self.database.execute(session, query)
    self.database.commit(session)

    # Pull out the created comment
    query = self.queryComment(comment_uuid)
    self.database.execute(session, query)

    return CommentRecord(self.database.fetch(session))
