# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses
from occam.commands.manager import command, option, argument

from occam.social.manager  import SocialManager
from occam.objects.manager import ObjectManager

@command('social', 'comment-list',
  category      = 'Social',
  documentation = "List comments that are attached to the given object.")
@argument("object", type="object", help="The object to pull comments for.")
@argument("comment_id", type=str, nargs="?", help="The comment id to particularly pull", default=None)
@option("-r", "--reply-to", dest    = "reply_to",
                            action  = "store",
                            help    = "Lists replied to this comment ID")
@option("-c", "--full-chain", dest    = "full_chain",
                              action  = "store_true",
                              help    = "Retrieves full ancestry for the given comment ID")
@uses(SocialManager)
@uses(ObjectManager)
class CommentListCommand:
  """ Implements the list command for the social component.
  """

  def do(self):
    Log.header("Listing comments")

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("Cannot find the requested object.")
      return -1

    comments = self.social.listCommentsWithReplies(obj, comment_id = self.options.comment_id, in_reply_to = self.options.reply_to, full_chain = self.options.full_chain)

    ret = [self.social.serializeComment(comment) for comment in comments]

    import json
    Log.output(json.dumps(ret))

    Log.done("Successfully listed comments")
    return 0
